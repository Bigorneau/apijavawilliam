import com.sun.net.httpserver.HttpServer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.awt.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;




public class Auth {
    private String redirect_uri = "http://localhost:8888";
    private Auth auth;
    private HttpServer server;
    private HttpClient client;

    private String clientId = "03435f4c004b54ae59bf44d25b76b911df5c61b98fd7e1ec93d86c063a3cba64";
    private String clientSecret ="447618b9f692b01d48bc80edfd020b49fcbf05c293a94038754c1aa885b880e8";
    private String scope ="read_api";




    // Client ID et secret good
    // voir req avec params , manque un params a getauthorized?

    public void getAuthorizationCode() {
        String url = "https://gitlab.com/oauth/authorize?";
        String parameters = url + "client_id=%s&redirect_uri=%s&response_type=code&scope=%s";
        String finalParameters = String.format(parameters, clientId, redirect_uri,scope);
        try {
            // Teste mon code
             Desktop.getDesktop().browse(new URI(finalParameters));
        } catch (URISyntaxException | IOException error) {
            System.out.println(error);
        }
    }


    // Server


    public  Auth(){
        client = HttpClient.newHttpClient();
    }


    public void runHttpServer() throws IOException {
        try {
            server = HttpServer.create(new InetSocketAddress("localhost",8888),0 );
            server.createContext("/", new MyHandler(this));
            server.start();
            System.out.println("serveur launched");
        }catch (Error error){
            System.out.println("oops, there was an error !!!");
        }
    }


    public void sendAuthCode(String code) {
//        server.stop(0);
        String url = "https://gitlab.com/oauth/token?";
        String parameters = url +"client_id=%s&client_secret=%s&code=%s&grant_type=authorization_code&redirect_uri=%s";
        String finalParameters = String.format(parameters,clientId,clientSecret,code,redirect_uri);
        try {
            // HttpClient client = HttpClient.newHttpClient()
            System.out.println("Next request");
            HttpRequest request =HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(finalParameters))
                    .uri(new URI(finalParameters))
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("Accept", "application/json").build();
            HttpResponse<String> response = client.send(request,HttpResponse.BodyHandlers.ofString());
            String content = response.body();

            JSONObject data = (JSONObject) JSONValue.parse(content);
            String accessToken = (String) data.get("access_token");
            String refreshToken = (String) data.get("refresh_token");
            System.out.println("Access token " + accessToken + "/" + "refresh Token " + refreshToken);
            callApi(accessToken);

        } catch (IOException | InterruptedException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void callApi(String accessToken){
        try {
            HttpRequest request = HttpRequest.newBuilder().GET()
                    .uri(new URI("https://gitlab.com/api/v4/projects?owned=true"))
                    .header("Accept", "application/json")
                    .header("Authorization", "Bearer "+ accessToken)
                    .build();
            System.out.println("Before response callApi");

            HttpResponse<String> response = client.send(request,HttpResponse.BodyHandlers.ofString());


            JSONArray data = (JSONArray) JSONValue.parse(response.body());
            for (int i=0;i<data.size();i++){
               // System.out.println(data.get(i));
                JSONObject myObject =(JSONObject) data.get(i);

                System.out.println(myObject.get("name"));
            }

            System.out.println(data);
            System.out.println("Should see the data");

        } catch (URISyntaxException|IOException |InterruptedException e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }
};


